# **CNN Summary**
### _In neural networks, Convolutional neural network (ConvNets or CNNs) is one of the main categories to do images recognition, images classifications. Objects detections, recognition faces etc., are some of the areas where CNNs are widely used._
![](https://www.jeremyjordan.me/content/images/2017/07/Screen-Shot-2017-07-26-at-1.44.58-PM.png)

# Complete CNN Architecture
![](https://miro.medium.com/max/700/1*4GLv7_4BbKXnpc6BRb0Aew.png)

### _The below figure is a complete flow of CNN to process an input image and classifies the objects based on values._
![](https://miro.medium.com/max/700/1*XbuW8WuRrAY5pC4t-9DZAQ.jpeg)

## Convolution Layer
Convolution is the first layer to extract features from an input image. Convolution preserves the relationship between pixels by learning image features using small squares of input data. It is a mathematical operation that takes two inputs such as image matrix and a filter or kernel.
![](https://miro.medium.com/max/461/1*kYSsNpy0b3fIonQya66VSQ.png)

![](https://miro.medium.com/max/413/1*4yv0yIH0nVhSOv3AkLUIiw.png) 

![](https://miro.medium.com/max/268/1*MrGSULUtkXc0Ou07QouV8A.gif)

## Strides
Stride is the number of pixels shifts over the input matrix. When the stride is 1 then we move the filters to 1 pixel at a time. When the stride is 2 then we move the filters to 2 pixels at a time and so on.

![](https://miro.medium.com/max/695/1*nGHLq1hx0gt02OK4l8WmRg.png)

## Padding
Sometimes filter does not fit perfectly fit the input image. We have two options:

- Pad the picture with zeros (zero-padding) so that it fits

- Drop the part of the image where the filter did not fit. This is called valid padding which keeps only valid part of the image.

## Non-Linearity(ReLU)
ReLU stands for Rectified Linear Unit for a non-linear operation. The output is ƒ(x) = max(0,x).

![](https://miro.medium.com/max/449/1*gcvuKm3nUePXwUOLXfLIMQ.png)