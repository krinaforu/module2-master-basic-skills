# **Face Recognition Summary**
![](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTuX2QSy46GUMLDCYAcO-f9UKCGAxXaAuu0rw&usqp=CAU)

## **How face recognition works?**
### Facial recognition is the process of identifying or verifying the identity of a person using their face. It captures, analyzes, and compares patterns based on the person's facial details.

### - The **face detection** process is an essential step as it detects and locates human faces in images and videos.

### - The **face capture** process transforms analog information (a face) into a set of digital information (data) based on the person's facial features.

### - The **face match** process verifies if two faces belong to the same person.

## **What is facial recognition used for?**
### **1. Security - law enforcement**
![](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSgFpy4AjvFLIkEdRM4RtMCQ0K4Xj2V3cZwoQ&usqp=CAU)
  
- **Find missing children and disoriented adults**
  
- **Identify and find exploited children**
  
- **Identify and track criminals**

- **Support and accelerate investigations**

### **2. Health**
![](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSyRI_3a701gNgasbnfD9rhFWIkhyyk0Q35Qg&usqp=CAU)

- **Track a patient's use of medication more accurately**

- **Detect genetic diseases such as DiGeorge syndrome with a success rate of 96.6%**

- **Support pain management procedures.**

# **A burgeoning market for facial recognition**
Without artificial intelligence development to back it up, facial recognition would not have gained traction. The market is expected to attain growth rate of 13% by 2023 to exceed market value of $ 7 billion by 2024. First used in army intelligence, this technology’s use is now more widespread:

- **Government**: maintain records of citizens, criminals and to identify missing persons

- **Banking**: A securer way of authentication of credit and debit cards, fraud prevention, maintaining database of known criminals and fraudsters

- **Business workplaces**: Access control, with fingerprint biometrics being replaced by facial recognition

- **Education**: attendance tracking, exams

- **Entertainment**: Access to halls

- **Travel**: Identification and paperless travel, facial registration of travelers and visitors at airports, railway stations

- **Automotive**: cars recognize owners and start only with face authentication

- **Medicine**: Identification of rare genetic diseases and diagnosis based on facial symptoms
